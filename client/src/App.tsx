import React from "react";
import io from "socket.io-client";

const App: React.FC = () => {
    const socket = io.connect("127.0.0.1:8081");
    socket.send('{"id": "jo"}\n');
    socket.on("message", console.log);
    return <div className="App"></div>;
};

export default App;
