package main

import (
    "gitlab.com/arnovanliere/ligilo/server/socket"
)

func main() {
    socketServer := socket.NewServer()

    // Start the servers
    go func() {
        socketServer.Start()
    }()

}
