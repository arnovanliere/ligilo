module gitlab.com/arnovanliere/ligilo/server

go 1.14

replace (
	"gitlab.com/arnovanliere/ligilo/server/socket" => "/socket"
)

require (
	github.com/gorilla/websocket v1.4.2
)
