package socket

import (
    "fmt"
    "log"
    "net/http"
    "os"

    "gitlab.com/arnovanliere/ligilo/server/common"
)

type Server struct {
    hub  *Hub
    port int
}

func NewServer() *Server {
    portStr := interface{}(os.Getenv("PORT"))
    port, ok := portStr.(int)
    if !ok {
        port = common.SocketServerPort
    }

    hub := newHub()
    go hub.run()
    return &Server{
        hub:  hub,
        port: port,
    }
}

func (s *Server) Start() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        serveWs(s.hub, w, r)
    })
    err := http.ListenAndServe(fmt.Sprintf(":%d", s.port), nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
