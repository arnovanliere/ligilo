package notification

type Notification struct {
    message string
}

func NewNotification(message string) *Notification {
    return &Notification{
        message: message,
    }
}

func (n *Notification) GetMessage() string {
    return n.message
}
