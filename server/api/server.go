package api

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "os"

    "gitlab.com/arnovanliere/ligilo/server/common"
    "gitlab.com/arnovanliere/ligilo/server/notification"
    "gitlab.com/arnovanliere/ligilo/server/socket"
)

type Server struct {
    port         int
    socketServer *socket.Server
}

// Create an API-server
func NewServer(server *socket.Server) *Server {
    portStr := interface{}(os.Getenv("PORT"))
    port, ok := portStr.(int)
    if !ok {
        port = common.ApiServerPort
    }
    return &Server{
        port:         port,
        socketServer: server,
    }
}

// Serve HTTP-requests
func (s *Server) Start() {
    fmt.Println("API-server starting...")
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        s.ForwardNotification(w, r)
    })
    err := http.ListenAndServe(fmt.Sprintf(":%d", s.port), nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}

// Forward incoming notifications to correct socket
func (s *Server) ForwardNotification(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodPost {
        common.SendErrorHTTP(w, common.InternalServerError)
        return
    }
    // Read the body
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        common.SendErrorHTTP(w, common.InternalServerError)
        return
    }

    // Convert from JSON to map
    var jsonData map[string]interface{}
    err = json.Unmarshal(body, &jsonData)
    if err != nil {
        common.SendErrorHTTP(w, common.InternalServerError)
        return
    }

    // Get the socket-id
    id, exists := jsonData["id"]
    if !exists {
        common.SendErrorHTTP(w, common.InternalServerError)
        return
    }

    // Get the message
    message, exists := jsonData["message"]
    if !exists {
        common.SendErrorHTTP(w, common.InternalServerError)
        return
    }

    // Send the notification over the correct socket
    msg := notification.NewNotification(message.(string))
    err = s.socketServer.ForwardNotification(id.(string), msg)
    if err != nil {
        common.SendErrorHTTP(w, common.ClientNotFound)
        return
    }
    w.WriteHeader(http.StatusCreated)
}

