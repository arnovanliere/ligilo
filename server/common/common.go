package common

import (
    "bufio"
    "encoding/json"
    "fmt"
    "net"
    "net/http"
)

type ErrorCode string

const (
    NoIdProvided        ErrorCode = "NO_ID_PROVIDED"
    ClientNotFound      ErrorCode = "CLIENT_NOT_FOUND"
    InternalServerError ErrorCode = "INTERNAL_SERVER_ERROR"
)

const (
    ApiServerPort    = 8080
    SocketServerPort = 8081
)

func SendErrorHTTP(w http.ResponseWriter, error ErrorCode) {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusInternalServerError)
    _, _ = w.Write([]byte(`{"error": "` + string(error) + `"}`))
}

// Send an error over a socket.
func SendErrorSocket(c net.Conn, error ErrorCode) error {
    data := map[string]interface{}{
        "error": error,
    }
    return SendJSON(c, data)
}

// Send a map as JSON over a socket.
func SendJSON(c net.Conn, data map[string]interface{}) error {
    // Create JSON-response
    str, err := json.Marshal(data)
    if err != nil {
        return err
    }

    // Send JSON-response
    _, err = fmt.Fprint(c, fmt.Sprintf("%s\n", str))
    return err
}

// Read JSON until a line-break.
func ReadJSON(c net.Conn) (map[string]interface{}, error) {
    // Read a string until a line-break
    str, err := bufio.NewReader(c).ReadString('\n')
    if err != nil {
        return nil, err
    }

    // Convert from JSON
    var data map[string]interface{}
    err = json.Unmarshal([]byte(str), &data)
    if err != nil {
        return nil, err
    }
    return data, nil
}
